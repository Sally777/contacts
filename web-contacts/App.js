import React from 'react';
import { StyleSheet, View } from 'react-native';
import {Route, Switch, BrowserRouter, NavLink} from 'react-router-dom';


export default class App extends React.Component {
  render() {

    return (
      <View style={styles.container}>
        <BrowserRouter>
          <Switch>
            <Route path="/" exact render={() =>
                <div className="App">
                  <h1 style={{marginLeft: '-70%'}}>Contacts</h1>
                  <button style={{borderRadius: '3px', fontSize: '20px', marginRight: '-40%'}}><NavLink to='/newContact'>Add new contact</NavLink></button>
                  {contacts}
                </div>
            } />
            <Route path="/newContact" exact component={AddNewContact} />

          </Switch>
        </BrowserRouter>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

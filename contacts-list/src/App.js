import React, {Component, Fragment} from 'react';
import {Route, Switch, BrowserRouter, NavLink} from 'react-router-dom';
import {fetchContacts} from "./store/actions";
import connect from "react-redux/es/connect/connect";
import AddNewContact from "./containers/AddNewContact";
import './App.css';
import ModalWindow from "./components/ModalWindow";


class App extends Component {

    state={
        show: false
    };

    showHandler = id => {
        this.setState({show: id})
    };

    hideModal = () => {
        this.setState({show: false})
    };

    componentDidMount() {
        this.props.fetchContacts();
    }

    render() {
        if (!this.props.contacts) {
            return <div>Please wait</div>
        }

        const contacts = Object.keys(this.props.contacts).map(contactId => {

            let contact = this.props.contacts[contactId];

            return (
                <Fragment>
                    <ModalWindow hide={this.hideModal} show={this.state.show === contactId}>
                        <p style={{marginRight: '-15%', fontSize: '30px'}}><img src={contact.photo} style={{width: '100px', height: '100px', border: '1px solid gray'}} />{contact.name}</p>
                    </ModalWindow>
                <div onClick={()=> this.showHandler(contactId)} style={{margin: '10px 0 10px 100px', width: '400px', border: '1px solid gray', borderRadius: '15px', background: '#a1c0ff', boxShadow: '0 0 10px'}}>

                    <p style={{marginRight: '-15%', fontSize: '30px'}}>
                        <img src={contact.photo} style={{width: '100px', height: '100px', border: '1px solid gray'}} />{contact.name}</p>
                </div>
                </Fragment>
            )

        });




    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact render={() =>
                    <div className="App">
                        <h1 style={{marginLeft: '-70%'}}>Contacts</h1>
                        <button style={{borderRadius: '3px', fontSize: '20px', marginRight: '-40%'}}><NavLink to='/newContact'>Add new contact</NavLink></button>
                        {contacts}
                    </div>
                } />
                <Route path="/newContact" exact component={AddNewContact} />

            </Switch>
        </BrowserRouter>
    );
  }
}

const mapStateToProps = state => ({
    contacts: state.contacts
});

const mapDispatchToProps = dispatch => ({
    fetchContacts: () => dispatch(fetchContacts())
});

export default connect(mapStateToProps,mapDispatchToProps)(App);

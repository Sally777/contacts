import axios from 'axios';
export const FETCH_CONTACTS_SUCCESS = 'FETCH_CONTACTS_SUCCESS';

const BASE_URL = 'https://web-contacts-e31bd.firebaseio.com/';

export const fetchContactsSuccess = contacts => ({type: FETCH_CONTACTS_SUCCESS, contacts});

export const fetchContacts = () => {
    return dispatch => {
        axios.get(BASE_URL + 'contact.json').then(
            response => {
                dispatch(fetchContactsSuccess(response.data));
            }
        );
    }
};
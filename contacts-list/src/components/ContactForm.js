import React, {Component} from 'react';

class ContactForm extends Component {
    state = {
        name: '',
        phone: '',
        email: '',
        photo: ''
    };

    valueChanged = event => {
        const {name, value} = event.target;

        this.setState({[name]: value});
    };


    submitHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state})
    };

    render() {
        return (
                <form className="ContactForm" onSubmit={this.submitHandler}>
                    <input type="text" name="name" placeholder="your full name"
                           onChange={this.valueChanged} value={this.state.name}
                    />
                    <input type="number" name="phone" placeholder="phone number"
                           onChange={this.valueChanged} value={this.state.phone}
                    />
                    <input type="text" name="email" placeholder="email"
                           onChange={this.valueChanged} value={this.state.email}
                    />
                    <input type="url" name="photo" placeholder="photo url"
                             onChange={this.valueChanged} value={this.state.photo}
                    />


                    <button type="submit">Save</button>
                </form>
        );
    }
}

export default ContactForm;
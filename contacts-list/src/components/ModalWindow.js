import React from 'react';

import './ModalWindow.css';
const ModalWindow = props => {
    return (
        <div onClick={props.hide} className="ModalWindow"
        style={{
            transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: props.show ? '1' : '0'
        }}>
            {props.children}
        </div>
    );
};

export default ModalWindow;
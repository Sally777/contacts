import React, {Component, Fragment} from 'react';
import ContactForm from "../components/ContactForm";
import {AxiosInstance as axios} from "axios";

class AddNewContact extends Component {

    addContact = contact => {
        axios.post('contact.json', contact).then(() => {
            this.props.history.replace('/')
        });
    };

    render() {
        return (
            <div>
                <Fragment>
                    <h1>Add new contact</h1>
                    <ContactForm onSubmit={this.addContact}  />
                </Fragment>
            </div>
        );
    }
}

export default AddNewContact;